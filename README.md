Am implementat un serviciu WCF folosind .NET framework 4.5 si Visual Studio 2013. Codul sursa poate fi accesat la adresa: https://bitbucket.org/alexolteanuinc/docprocessingwcf


# Structura proiectului #

 Proiectul contine:
-	Codul aferent serviciului WCF, sub forma a doua fisiere: IFileProcessingService.cs si FileProcessingService.svc.cs
-	Codul aferent logicii pentru incarcarea fisierelor in memorie, in directorul FileLoaders
-	Codul aferent logicii pentru analiza continutului, in directorul FileAnalyzer
-	Fisiere de test in directorul App_Data (un fisier txt si unul pdf)

Serviciul FileProcessingService primeste ca parametru fisierul ce trebuie procesat, apoi incarca continutul acestuia in memorie si il analizeaza, returnand rezultate sub forma de text. Pentru aceasta demonstratie, se presupune ca fisierele se gasesc deja intr-o locatie pe server, dar solutia poate fi extinsa pentru a permite incarcarea de fisiere de pe client pe server.
Structura serviciului FileProcessingService abstractizeaza conceptele de FileLoader si FileAnalyzer si le foloseste conform modelului de programare Pipeline. Serviciul va incerca, pe rand, fiecare FileLoader oferit de programator pana va gasi unul care reuseste sa citeasca fisierul dat ca parametru de intrare. Apoi va aplica asupra continutului fisierului fiecare FileAnalyzer pe rand.
Am implementat doua variante de FileLoader, unul pentru fisiere txt si unul pentru fisiere pdf. Am realizat citirea PDF-urilor folosind biblioteca iTextSharp[1]. Aceasta  dat fiind aspectul de demonstratie al aplicatiei.
Am implementat doua variante de FileAnalyzer. KeywordSearchAnalyzer va filtra liniile de text si le va pastra doar pe acelea care contin un anumit cuvant cheie. KeywordCountAnalyzer va numara cate linii de text contin cuvantul cheie.


# Testarea aplicatiei #

Pentru testarea aplicatiei am folosit WCF Test Client si cele doua fisiere care se regasesc in directorul App_Data. Am configurat o instanta a KeywordSearchAnalyzer pentru cuvantul cheie „tenis” si o alta instanta pentru cuvantul cheie „Simona”. De asemenea, am configurat o instanta a KeywordCountAnalyzer pentru cuvantul cheie „Simona”. 
Figura de mai jos arata rezultatul obtinut pentru fisierul txt. Se remarca 2 rezultate pentru cuvantul cheie „tenis” si 5 rezultate pentru cuvantul cheie „Simona”.
 
Figura de mai jos arata rezultatul obtinut pentru fisierul pdf. Din cauza bibliotecii de citire a PDF rezultatele sunt diferite fata de fisierul txt, desi textul utilizat la generarea PDF este acelasi. Se remarca 1 rezultat pentru cuvantul cheie „tenis” si 2 rezultate pentru cuvantul cheie „Simona”
 

# Extinderea aplicatiei #

3.1.	Procesari noi
Pentru a adauga noi procesari, un programator trebuie sa faca doua lucruri:
-	Sa adauge o clasa care sa implementeze IFileAnalyzer, de preferinta in directorul FileAnalyzer
-	Sa adauge una sau mai multe instante ale acestei clase in cadrul serviciului in lista denumita FileAnalyzers
Asa cum se poate observa in cazul procesarilor existente, acestea pot fi construite in asa fel incat sa primeasca si alti parametrii decat fisierul, spre exemplu un cuvant cheie. 
3.2.	Configurarea parametrilor pentru FileAnalyzer
Solutia actuala presupune configurarea de catre programator a parametrilor pe care ii foloseste un FileAnalyzer. Solutia poate fi extinsa in vederea incapsularii acestor parametrii intr-o clasa destinata setarilor sau intr-un fisier de configurarea, si apoi pot fi expusi catre o interfata cu utilizatorul.
3.3.	Upload
Solutia actuala presupune ca fisierele se gasesc deja pe server. Poate fi dezvoltata o logica bazata pe streams sau buffers pentru a incarca fisierele de pe client pe server. Totodata, aceasta presupune realizarea unei aplicatii client, alta decat WCF Test Client, care sa ofere sprijin pentru selectarea unui fisier de pe client.
3.4.	Citirea PDF-urilor
Solutia actuala foloseste o biblioteca relativ mica pentru a citi fisiere in format PDF. Ea poate fi extinsa prin folosirea unei alte biblioteci, mai cuprinzatoare.


# Consideratii privind fiabilitatea solutiei #

Considerand ca SDK-ul pentru citirea PDF-ului ofera rezultate inconsistente, diferite de la o rulare la alta, are sens sa se incerce de mai multe ori incarcarea fisierului prin SDK, in cazul in care nu functioneaza de prima oara. Astfel:
-	se poate defini un retry counter pentru actiunea de incarcare a unui fisier pdf in memorie, daca se depaseste un numar de reincercari, se renunta la incarcare si utilizatorul este notificat
-	in cazul in care SDK poate oferi rezultate partiale, se poate vedea calitatea rezultatului incarcarii (e.g. cat la suta din fisier a fost incarcat) si, in cazul in care nu se obtine 100%, se retine rezultatul cel mai bun; daca se obtine un rezultat cu un procent mai mare decat un prag configuratbil sau dupa ce expira numarul de incercari se ofera utilizatorului decizia de a continua procesarea in acest context sau de a renunta la procesare
-	se poate cerceta care sunt conditiile in care se obtine calitatea cea mai buna si se poate incerca crearea acestor conditii, spre exemplu daca SDK nu ofera rezultate bune la incarcari mari ale procesorului, se poate impune o limitare cu privire la numarul de procesari paralele (threaduri) care se desfasoara in acelasi timp, pe fisiere diferite

Referinte
[1] iTextSharp: http://www.squarepdf.net/file/get/guakzlu2rza6zarkqk5545hzf4