﻿using FileProcessingSolution.FileAnalyzer;
using FileProcessingSolution.FileLoaders;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace FileProcessingSolution
{
    public class FileProcessingService : IFileProcessingService
    {
        public List<string> ProcessFile(string filePath)
        {
            List<IFileLoader> fileLoaders = new List<IFileLoader>();
            List<IFileAnalyzer> fileAnalyzers = new List<IFileAnalyzer>();

            /*
             * ------------------------------------------------------------------------
             * configure your file loaders and analyzers here
             */

            fileLoaders.Add(new PdfFileLoader());
            fileLoaders.Add(new TxtFileLoader());

            fileAnalyzers.Add(new KeywordSearchAnalyzer("tenis"));
            fileAnalyzers.Add(new KeywordSearchAnalyzer("Simona"));
            fileAnalyzers.Add(new KeywordCountAnalyzer("Simona"));

            /*
             * end of configuration, no more changes should be needed beyond this point
             * ------------------------------------------------------------------------
             */

            // load the file

            List<string> text = new List<string>();
            foreach (IFileLoader fileLoader in fileLoaders)
            {
                    text = fileLoader.load(filePath);
                    if (text != null)
                    {
                        break; // only one file loader needs to work
                    }
            }
            if (text == null)
            {
                Console.WriteLine("No suitable file loader could be found");
                return null;
            }

            // analyze the file

            List<string> output = new List<string>();
            foreach (IFileAnalyzer fileAnalyzer in fileAnalyzers)
            {
                output.Add(">>> RESULTS FOR " + fileAnalyzer.getName());
                output.AddRange(fileAnalyzer.analyze(text));
            }
            return output;
        }
    }
}
