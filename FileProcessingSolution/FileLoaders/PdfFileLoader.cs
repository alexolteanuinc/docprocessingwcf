﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;

namespace FileProcessingSolution.FileLoaders
{
    public class PdfFileLoader : IFileLoader
    {
        public List<string> load(string filePath)
        {
            List<string> lines = new List<string>();
            try
            {
                using (PdfReader reader = new PdfReader(System.Web.Hosting.HostingEnvironment.MapPath("/App_Data/" + filePath)))
                {
                    StringBuilder text = new StringBuilder();

                    for (int i = 1; i <= reader.NumberOfPages; i++)
                    {
                        lines.Add(PdfTextExtractor.GetTextFromPage(reader, i));
                    }

                }
            }
            catch (Exception e)
            {
                Console.WriteLine("The file could not be read:");
                Console.WriteLine(e.Message);
                return null;
            }
            return lines;
        }
    }
}