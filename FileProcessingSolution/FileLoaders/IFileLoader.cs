﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FileProcessingSolution.FileLoaders
{
    public interface IFileLoader
    {
        List<string> load(string filePath);
    }
}