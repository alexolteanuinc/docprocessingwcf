﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace FileProcessingSolution.FileLoaders
{
    public class TxtFileLoader : IFileLoader
    {
        public List<string> load(string filePath)
        {
            List<string> lines = new List<string>();
            try
            {
                using (StreamReader sr = new StreamReader(System.Web.Hosting.HostingEnvironment.MapPath("/App_Data/" + filePath)))
                {
                    string line;
                    while((line = sr.ReadLine()) != null) {
                        lines.Add(line);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("The file could not be read:");
                Console.WriteLine(e.Message);
                return null;
            }
            return lines;
        }
    }
}