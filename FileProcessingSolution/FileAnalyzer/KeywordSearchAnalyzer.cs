﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FileProcessingSolution.FileAnalyzer
{
    public class KeywordSearchAnalyzer : IFileAnalyzer
    {
        private string keyword;

        public KeywordSearchAnalyzer(string keyword)
        {
            this.keyword = keyword;
        }

        public List<string> analyze(List<string> input)
        {
            List<string> output = new List<string>();
            foreach (string line in input)
            {
                if (line.Contains(keyword))
                {
                    output.Add(line);
                }
            }
            return output;
        }

        public string getName()
        {
            return "keyword search for " + keyword;
        }
    }
}