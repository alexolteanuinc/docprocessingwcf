﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FileProcessingSolution.FileAnalyzer
{
    public class KeywordCountAnalyzer : IFileAnalyzer
    {
        private string keyword;

        public KeywordCountAnalyzer(string keyword)
        {
            this.keyword = keyword;
        }

        public List<string> analyze(List<string> input)
        {
            List<string> output = new List<string>();
            int count = 0;
            foreach (string line in input)
            {
                if (line.Contains(keyword))
                {
                    count++;
                }
            }
            output.Add(count.ToString());
            return output;
        }

        public string getName()
        {
            return "keyword search for " + keyword;
        }

    }
}