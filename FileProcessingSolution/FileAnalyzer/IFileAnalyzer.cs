﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FileProcessingSolution.FileAnalyzer
{
    public interface IFileAnalyzer
    {
        List<string> analyze(List<string> input);

        string getName();
    }
}